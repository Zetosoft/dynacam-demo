# dynacam-demo

A detailed example project for the dynacam plugin. Use arrow keys to rotate and accelerate.

Copyright (c) 2019-2020, Zetosoft
All rights reserved.
